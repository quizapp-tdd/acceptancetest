const { goto, text, currentURL, waitFor } = require('taiko');

// Given: That I am on "http://localhost:8080"
step('When: The page is loaded totally <link>', async (link) => {
	waitFor(2000);
	const url = await currentURL();
	if (url !== link) throw new Error(`Current URL must be ${link}.`);
});

step('Then: I should see <name> button', async (name) => {
	await button(name).exists();
});

// Given : That i see "Add Question" button

step('Then: I should land on <link> page', async (link) => {
	const url = await currentURL();
	if (url !== link) throw new Error(`Current URL must be ${link}.`);
});

beforeScenario(async () => {
	await goto('http://localhost:8080');
});
