/* globals gauge*/

const path = require('path');
const { openBrowser, closeBrowser, screenshot } = require('taiko');

beforeSuite(async () => {
	await openBrowser({
		headless: true,
	});
});

afterSuite(async () => {
	await closeBrowser();
});

// Return a screenshot file name
gauge.customScreenshotWriter = async function () {
	const screenshotFilePath = path.join(
		process.env['gauge_screenshots_dir'],
		`screenshot-${process.hrtime.bigint()}.png`,
	);

	await screenshot({
		path: screenshotFilePath,
	});
	return path.basename(screenshotFilePath);
};

step('Open <link>', async (link) => {
	await goto(link);
});

step('When: I click <name> button', async (name) => {
	await click(button(name));
});

step('Then: I should see <name> header', async (name) => {
	await text(name).exists();
});
