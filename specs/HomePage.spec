# Home page acceptance test

* Open "http://localhost:8080"

## Given: That I am on "http://localhost:8080"
* When: The page is loaded totally "http://localhost:8080"
* Then: I should see "Quiz App" header
* Then: I should see "Add Question" button
* Then: I should see "Start Quiz" button

## Given : That i see "Add Question" button
* When: I click "Add Question" button
* Then: I should land on "http://localhost:8080/add" page

## Given : That i see "Start Quiz" buttton
* When: I click "Start Quiz" button
* Then: I should land on "http://localhost:8080/quiz" page
