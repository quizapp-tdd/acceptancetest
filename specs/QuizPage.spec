# Quiz page acceptance test

* Open "http://localhost:8080/quiz"

## Given: that "http://localhost:8080/quiz" isActive

* When: I land on "http://localhost:8080/quiz" i want to see my main components on this page
* Then: I should see "Quiz App" header
* Then: I should see -Process information- as "Please answer Questions downbelow and click Send button for see your Result"
* Then: I should see "Question" with unchecked answers component
* Then: I should see "Send" button inactive

## Given: I solve atleast a question
* When: I clicked a answer checkbox
* Then: I should see "Send" become active

## Given: I finished my quiz

* When: I click "Send"
* Then: I should see all "Question" become invisible
* Then: I should see -Process information- change to "Your result: 1"
